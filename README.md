## Clone repository:
```bash
git clone https://github.paypal.com/GPS-EMEA-Integrations/wordpress-docker.git
```

## Run docker-compose.yml

```bash
docker-compose up -d
```

## Increase limit for uploading files in wordpress

```bash
docker exec -it wordpress /bin/bash
```

## Install nano
```bash
apt update
```
```bash
apt install nano
```

## modify '.htaccess' file
```bash
nano .htaccess
```
### Add this lines at the end of the file
```bash
php_value upload_max_filesize 125M
php_value post_max_size 125M
php_value max_execution_time 3000
php_value max_input_time 3000
```

## System is ready to upload plugins up to 125Mb

## WooCommerce plugin:
- https://wordpress.org/plugins/woocommerce/
## PayPal PPCP plugin:
- https://woocommerce.com/products/woocommerce-paypal-payments/


## Accessing Wordpress
http://localhost:8090

## Accessing admin area
http://localhost:8090/admin